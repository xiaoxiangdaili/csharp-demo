# 小象代理API Demo - C#

## 使用说明

本示例项目中，使用C#常用的http类库，您可根据实际项目情况选择对应类库代码进行参考：
- WebRequest


示例代码在替换常量appKey，appSecret为实际应用账号、密码之后，均可直接运行。

## 技术支持

如果您发现代码有任何问题, 请提交Issue。

欢迎提交Pull request以使代码样例更加完善。